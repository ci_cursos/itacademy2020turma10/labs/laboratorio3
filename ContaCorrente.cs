namespace laboratorio3
{
    public class ContaCorrente {
        private decimal saldo;
        private string observacoes;
        public void Depositar(decimal val)
        {
            saldo = saldo + val; //saldo += val
        }
        public void Sacar(decimal val)
        {
            saldo = saldo - val;
        }
        public decimal Saldo
        {
            get
            {
                return saldo;
            }
        }

        public string Observacoes
        {
            get
            {
                return observacoes;
            }
            set
            {
                observacoes = value;
            }
        }

        public string Agencia {get; private set;}

        public ContaCorrente(decimal val, string codAgencia)
        {
            saldo = val;
            Agencia = codAgencia;
        }

        public ContaCorrente() : this(0,"01",string.Empty)
        {
        }

        public ContaCorrente(decimal val, string codAgencia, string obs)
        {
            saldo = val;
            observacoes = obs;
            Agencia = codAgencia;
        }
    }
}